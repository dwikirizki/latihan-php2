<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>
        <?php 
            // soal1
            echo "<h3>Soal 1</h3>";
            echo "<h4>Looping Pertama</h4>";
                for ($k=2; $k<=20; $k+=2)
                {
                    echo $k. " I Love PHP <br>";
                }
            // soal1b 
            echo "<h4>Looping Kedua</h4>";
                for ($l=20; $l>=2; $l-=2)
                {
                    echo $l. " I Love PHP <br>";
                }
            // soal2
            echo "<h3>Soal No 2 Looping Array Modulo </h3>";
            $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        echo "<br>";
        echo " Array sisa baginya adalah: ";
       foreach ($numbers as $value){
        $rest[] = $value %= 5;
       }
            print_r($rest);
        echo "<br>";
        // soal3
        echo "<h3>Soal No 3 Looping Asociative Array</h3>";
        $biodata = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg'],
        ];
        foreach($biodata as $key => $value){
            $item = array (
                'id' => $value[0], 
                'price' => $value [2],
                'name' => $value [1],
                'description' => $value [3],
                'source' => $value [4],
            );
            print_r ($item);
            echo "<br>";
        }
        // soal4
        echo "<h3>Soal No 4 Asterix </h3>";
        for ($u=1; $u<=5; $u++){
            for ($a=1; $a<=$u; $a++){
            echo "*";
        }
            echo "<br>";
        }
        ?>
</body>
</html>